# Uncommon Lisp

This project contains the formal definition and interpreter for a toy language
called Uncommon Lisp. The purpose of this project is to explore what it takes to
produce a minimal Lisp-style language that is Turing complete.

## Definition

Uncommon Lisp is an axiomatically defined formal language. It revolves around
so-called S-exps, the iconic Lisp syntax. We refer to expressions with side
effects as statements. In Uncommon Lisp, expressions may contain statements and
vice versa.

### Syntax

We define Uncommon Lisp with EBNF as follows. Tokens must be separated with one
or more whitespace characters. There are two natural ways to define the syntax.
First, we define common productions for both grammars:
```EBNF
literal = numeral
        | string;

numeral = ? ..., -2, -1, 0, 1, 2, ... ?;
string = ? a string literal enclosed in double quotation marks (`"`) ?;

symbol = name
       | quote;

name = character, { character };

character = ? An ASCII or Unicode character ? - reserved character;
reserved character = '('
                   | ')'
                   | "'"
                   | whitespace character;
whitespace character = ? any whitespace character ?;

quote = "'", s-exp;

argument list = '(', { symbol }, ')';
```

#### Unofficial Definition

The first definition explains the names we use for our syntax fragments:

```EBNF
s-exp = literal
      | symbol
      | cons cell
      | special form;

list = '(', { s-exp }, ')';

special form = if expression
             | function
             | macro
             | head expression
             | tail expression
             | set statement
             | do statement;

if expression = '(', 'if', s-exp, s-exp, s-exp, ')';
function = '(', 'lambda', argument list, s-exp, ')';
macro = '(', 'macro', argument list, s-exp, ')';
head expression = '(', 'head', s-exp, ')';
tail expression = '(', 'tail', s-exp, ')';
set statement = '(', 'set', name, s-exp, ')';
do statement = '(', 'do', { s-exp }, ')';
```

This definition is intuitive, but we define another definition that will be the
official syntax of Uncommon Lisp.

#### Official Definition

The second definition has fewer production rules which makes the language easier
to work with mathematically:

```EBNF
s-exp = literal
      | non-reserved symbol
      | reserved symbol
      | list;

non-reserved symbol = symbol - reserved symbol;
reserved symbol = 'nil'
                | 'cons'
                | 'head'
                | 'tail'
                | 'lambda'
                | 'macro'
                | 'if'
                | 'set'
                | 'do';

cons cell = 'nil'
          | '(', 'cons', s-exp, s-exp, ')';
```

There is one notable difference between the two definitions though. The `list`
production of the unofficial syntac is a special case of the `cons cell`
production. Even if we acknowledge the `cons cell` production as the formally
canocical definition for lists, we use the `list` production as a convenient
form of syntactical sugar. Each list written as
```
(e1 e2 ... en)
```
can be expanded mechanically into
```
(cons e1 (cons e2 (... (cons en nil))))
```
and vice versa, meaning that we have a natural isomorphism between these two
notations. The expansion from `list` to `cons cell` notation could be
implemented as a reader macro that will be expanded before the rest of macros.
Hence, the formal semantics don't need to care which notation is used. A runtime
system of Uncommon Lisp will only deal  with `cons cell` lists. This justifies
our relaxed level of formality on this matter.

### Semantics

We define the semantics of Uncommon Lisp in two stages: macro expansion and
`s-exp` evaluation. The evaluation semantics are strict but partial. There are
expressions for which the semantics defines no meaning under a given context.
A meaningless expression can be considered equivalent a to non-terminating
computation or an error state (i.e. runtime error).

We use `f(x1,x2,...,xn)` to denote n-ary function (definition/application) in
the usual mathematical sense in our metalogic. The notation `(f x1 x2 ... xn)`
refers to an Uncommon Lisp expression. We use `X := Y` to express that we define
`X` in terms of `Y` (definitional equality), while we use `X = Y` to denote the
assertion "`X` equals `Y`".

#### Data Structures

NB: The operations `lookup`,`pop`, and `find` will be intentionally defined as
partial functions. A semantic rule cannot be applied if it would involve
undefined entities. The evaluation halts with error if the expression being
evaluated is not in normal form (to be defined later) and none of the semantic
rules can be applied to it. We consider expressions that trigger errors to be
semantically meaningless.

*Map* is a data structure that comes with operations `lookup` and `upsert`. A
map contains pairs of elements known as *keys* and the *values*. We use `name`s
as keys and `s-exp`s as values. We use capital letters `M` and `N` for denoting
maps. Lower case letters `x` and `y` denote keys while `v`, `w`, and `z` denote
values. We use the set-theoretic notation `{}` to denote the empty map and
`{(x1,v1), (x2,v2), ... (xn,vn)}` to denote a map with `n` key-value pairs. A
*key-value pair* `(x,v)` represents a (semantic) `binding` of name `x` to value
`v`.

For each map `M`, key `x`, and value `v`, we define that:
1. `M` contains exactly zero or one pairs `(x,v)`.
2. If the pair `(x,v)` is in `M`, then `lookup(M,x) := v`.
3. We define `upsert(M,x,v)` in parts:
   a. If `(x,v)` is in `M`, then `upsert(M,x,v) := M`.
   b. If there is no `z` such that `(x,z)` is in `M` (i.e. `M` does not contain
      a value for the key `x`), then `upsert(M,x,v) := N` is the least map
      containing `(x,v)` as well as every key-value pair of `M`. (That is, `N`
      is a map such that `(x,v)` is in `N`, and for each `y` and `w` such that
      `x /= y` and `v /= w`, `(y,w)` is in `N` if and only if it is in `M`.)
   c. If there is `z` such that `(x,z)` is in `M`, then `upsert(M,x,v) = N`
      where `N` is the least map that contains `(x,v)` as well as

Definition 1 states that a map represents a (finite) functional relation.
Definition 2 states that `lookup` is a partial function that returns the image
`M(x)` of key `x` under this relation `M`. Definition 3 states that `upsert`
adds the given key-value pair into the map discarding the previous binding if it
exists.

For storing function arguments, our model uses a stack. `Stack` is a recursive
data structure that comes with operations `push`, `pop`. We use the notation
`[]` to denote the empty stack, and `[M,S]` to denote a stack that contains map
`M` and stack `S`. (`M` is stacked on top of `S` just like a plate can be
stacked on top of a stack of other plates.) We use `[Mn, ..., M2, M1]` to denote
the stack `[Mn, [..., [M2, [M1, []]]]]`.

For each stack `S` and map `M`, we define that:
4. `push(S,M) := [M,S]`.
5. `pop([M,S]) := S` (NB: `pop([])` is left undefined).

NB: We could alternatively define maps as stacks of key-value pairs. We leave
the definitions of `upsert` and `lookup` in this scenario as an exercise to the
reader.

We finally define function `find` for every stack `S`, map `M`, key `x`, and
value `v`:
6. `find([M,S],x) := v` if `(x,v)` is in `M`.
7. `find([M,S],x) := find(S,x)` if `(x,v)` is not in `M`.

Definitions 6 and 7 state that the `find` operation returns the value `v` for
the key `x` in the topmost map of the given stack that contains `(x,v)` if such
a binding exists.

Our semantics relies on one stack and one non-stack map which is called the
`heap`. Stack is used for storing the values of local variables defined either
as function arguments or `let` bindings. Heap is used for storing mutable global
variable values that can be (re)defined using `set` expressions.

Note that using `set` expressions may lead into Uncommon Lisp programs that
behave differently depending on evaluation order or the presence of multiple
threads. The `set` expression is the only expression with side-effects, so
removing `set` would produce a purely functional language.

#### Semantic Rules

We use a map for containing bindings introduced with `set` forms. This map has
a special name: *heap*. We use a stack of maps known as *stack frames* for
storing bindings during function calls. We refer to heaps with `H` and `K` and
to stacks with `S` and `T`.

We define the semantics of Uncommon Lisp through axioms and rules of inference.
We use ellipsis (`...`) to denote recursion on metalogical level. We use
numerical indices `1`, `2`, `3`, ... or symbolic indices `i`, `j`, `k`, ... in
postfix positions to denote distinct metavariables.

We use notation similar to that of natural inference or sequent calculus.
The notation
```
 H,S,a | c1, c2, ..., cn
------------------------- (X)
          K,T,b
```
means that, under metalogical (e.g. intuitionistic first order logic) conditions
`c1`, `c2`, ..., `cn`, we may conclude that given heap `H` and stack `S`, the
`s-exp` `a` evaluates to the `s-exp` `b` while producing heap `K` and stack `T`.
The heaps and stacks may be omitted when the rule does not depend on or change
them. We call the change of heap a *side effect*. `X` is a metalogical name for
the rule.

If the rule is always applicable, the vertical bar and the conditions will be
omitted. We use the metalogical notation `H,S,a --> K,T,b` to assert that `a`
with heap `H` and stack `S` evaluates to `b` while producing heap `K` and stack
`T` by applying evaluation rules a finite number of times and that all the
necessary conditions are met at each step and all partial function applications
are defined.

For macro expansion, we similarly use the notation `a ==> b` to denote the
proposition that `a` under into `b`. Macro expansion is always context-free
(as well as lazy) and therefore free of side effects. Hence, macros cannot
produce side effects in Uncommon Lisp.

The metalogical notation `e [x1 := a1, x2 := a2, ..., xn := an]` means
simultaneous substitution of variables `x1`, `x2`, ..., `xn` with expressions
`a1`, `a2`, ..., `an` inside expression `e` the way it is used in logic or
lambda-calculus. We use similar notation for contexts.

We call literals and `nil` *normal forms*.

#### Macro Expansion Rules

TODO

#### Evaluation Rules

The evaluation rules of Uncommon Lisp follow below:

```
 nil
----- (nil)
 nil

 e | e is a literal
-------------------- (literal)
         e

 'e
---- (quote)
  e

 H,S,x | x is a name
       , find(S,x) is defined
------------------------------ (find)
        H,S,find(S,x)

 H,S,x | x is a name
       , find(S,x) is not defined
       , lookup(H,x) is defined
---------------------------------- (lookup)
         H,S,lookup(H,x)

    H,S,(set x v)
--------------------- (set)
 upsert(H,x,v),S,nil

  (do)
-------- (nop)
  nil

 H,S,(do e1 e2 ... en) | H,S,e1 --> K,S,f
------------------------------------------ (do)
            K,S,(do e2 ... en)

 H, S, ((lambda x (x1 x2 ... xn) e) e1 e2 ... en)
   | ei --> vi for each 1 <= i <= n
   , each vi is in normal form
--------------------------------------------------- (call)
   H, push(S, {(x1,v1), (x2,v2), ..., (xn,vn)}), e

 (if p a b) | p --> nil
------------------------ (then)
           b

 (if p a b) | p --> v
            , v is a normal form other than nil
------------------------------------------------ (else)
                       a

 (head (cons x xs))
-------------------- (head)
         x

 (tail (cons x xs))
-------------------- (tail)
         xs
```

Note that the rules `head`, `tail`, and `quote` all fully lazy. The `if-1` and
`if-2` rules are only lazy in terms of `a` and `b`. Rest of the rules are
strict. Rules are only applicable when all of their conditions are defined and
decidable. This means that the evaluation semantics of Uncommon Lisp are
partial. We make no distinction between errors and non-termination.

## Theory

### TODO

1. Show that `cons cell` lists of the form `nil` or `(cons x (cons y (cons z ...)))`
   are equivalent with lists of the more compact form `(x y z ...)` of the
   second syntax definition.
2. Show that backquotes, splices, etc. can be derived from quotes and macros.
3. Show that some other basic stuff, such as `case` expressions and arrow macros
   can be defined.
4. Investigate if `(let (x v) e)` could be defined as a macro expanding to
   `((fun x e) v)`.
5. Show that Uncommon Lisp can express all pure untyped lambda terms or perhaps
   mu-recursive functions, i.e. demonstrate Turing completeness.
